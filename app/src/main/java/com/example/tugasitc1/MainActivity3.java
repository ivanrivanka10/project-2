package com.example.tugasitc1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity3 extends AppCompatActivity {
    private TextView email;
    private String email1;
    private TextView name;
    private String name1;
    private TextView major;
    private String major1;
    private TextView nim;
    private String nim1;
    private Button btn_find;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        btn_find = findViewById(R.id.btn_find2);
        email = findViewById(R.id.isi_email);
        email1 = getIntent().getStringExtra("a");
        email.setText(email1);
        name = findViewById(R.id.isi_nama);
        name1 = getIntent().getStringExtra("b");
        name.setText(name1);
        major = findViewById(R.id.isi_major);
        major1 = getIntent().getStringExtra("c");
        major.setText(major1);
        nim = findViewById(R.id.isi_nim);
        nim1 = getIntent().getStringExtra("d");
        nim.setText(nim1);
        btn_find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent klik3 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/asusrog.id/?hl=id"));
                startActivity(klik3);
            }
        });

    }
}